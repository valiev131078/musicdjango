from django import template

from user.models import *

register = template.Library()


@register.simple_tag()
def get_categories():
    return Category.object.all()

@register.inclusion_tag('user/list_categories.html')
def show_categories():
    categories = Category.object.all()
    return {"categories": categories}
