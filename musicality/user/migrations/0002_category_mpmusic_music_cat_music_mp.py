# Generated by Django 4.0.2 on 2022-02-11 14:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(db_index=True, max_length=100)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MPmusic',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(db_index=True, max_length=100)),
                ('mp', models.FileField(null=True, upload_to='photos/%Y-%m-%d/')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='music',
            name='cat',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='user.category'),
        ),
        migrations.AddField(
            model_name='music',
            name='mp',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='user.mpmusic'),
        ),
    ]
