from django.contrib import admin

# Register your models here.
from .models import *


class MusicAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'created_at', 'image', 'is_published')
    list_display_links = ('id', 'title')
    search_fields = ('title', 'id')
    list_editable = ('is_published',)
    list_filter = ('created_at', 'is_published')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'created_at')
    list_display_links = ('id', 'name')
    search_fields = ('name',)


class MPmusicAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'created_at', 'mp')
    list_display_links = ('id', 'name')
    search_fields = ('name',)


admin.site.register(Music, MusicAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(MPmusic, MPmusicAdmin)
