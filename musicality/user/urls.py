from django.urls import path

from .views import *

urlpatterns = [
    path('', index, name='home'),
    path('about/', about, name='about'),
    path('logout/', user_logout, name='logout'),
    path('addpage/', addpage, name='add_page'),
    path('login/', user_login, name='login'),
    path('register/', register, name='register'),
    path('post/<int:post_id>/', view_post, name='view_post'),
    path('search/', SearchResultsView.as_view(), name='search_results'),
    path('category/<int:cat_id>/', get_category, name='category'),
]
