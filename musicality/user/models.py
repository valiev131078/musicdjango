from django.db import models
from django.urls import reverse


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True


class Music(BaseModel):
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to="photos/%Y-%m-%d/", null=True)
    is_published = models.BooleanField(default=True)
    cat = models.ForeignKey('Category', on_delete=models.PROTECT, null=True)
    mp = models.ForeignKey('MPmusic', on_delete=models.CASCADE, null=True)

    def get_absolute_url(self):
        return reverse('view_post', kwargs={"post_id": self.pk})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Музыка'
        verbose_name_plural = 'Музыка'
        ordering = ['created_at', 'title']


class Category(BaseModel):
    name = models.CharField(max_length=100, db_index=True)

    def get_absolute_url(self):
        return reverse('category', kwargs={"cat_id": self.pk})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ['created_at']



class MPmusic(BaseModel):
    name = models.CharField(max_length=100, db_index=True)
    mp = models.FileField(upload_to="trek/%Y-%m-%d/", null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Треки'
        verbose_name_plural = 'Треки'
        ordering = ['created_at']
