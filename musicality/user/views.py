from django.http import HttpResponse, HttpResponseNotFound, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView
from django.contrib import messages
from django.contrib.auth import login, logout

from .forms import *
from .models import *

menu = [{'title': "О сайте", 'url_name': 'about'},
        {'title': "Добавить трек", 'url_name': 'add_page'},
        {'title': "Обратная связь", 'url_name': 'contact'},
        {'title': "Войти", 'url_name': 'login'}
]


def index(request):
    posts = Music.objects.all()
    context = {
        'posts': posts,
        'title': 'Главная страница',
        'cat_selected': 0,
    }

    return render(request, 'user/index.html', context=context)


def about(request):
    return render(request, 'user/about.html', {'menu': menu, 'title': 'О сайте'})


def addpage(request):
    if request.method == 'POST':
        form = AddPostForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = AddPostForm()
    return render(request, 'user/addpage.html', {'form': form, 'title': 'Добавить'})


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, 'Регистрация прошла успешно')
            return redirect('home')
        else:
            messages.error(request, 'Ошибка регистрации')
    else:
        form = UserRegisterForm()
    return render(request, 'user/register.html', {"form": form})


def user_login(request):
    if request.method == 'POST':
        form = UserLoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('home')
    else:
        form = UserLoginForm()
    return render(request, 'user/login.html', {"form": form})


def user_logout(request):
    logout(request)
    return redirect('login')


def pageNotFound(request, exeption):
    return HttpResponseNotFound('<h1>Страница не найдена</h1> ')


def show_post(request, post_id):
    post = get_object_or_404(Music, pk=post_id)
    context = {
        'posts': post,
        'title': post.title,
        'cat_selected': post.cat_id,
    }


def get_category(request, cat_id):
    posts = Music.objects.filter(cat_id=cat_id)
    category = Category.objects.get(pk=cat_id)

    context = {
        'posts': posts,
        'category': category,
    }

    return render(request, 'user/category.html', context=context)


def view_post(request, post_id):
    post_item = get_object_or_404(Music, pk=post_id)
    return render(request, 'user/view_post.html', {"post_item": post_item})


class SearchResultsView(ListView):
    model = Music
    template_name = 'user/search_results.html'

    def get_queryset(self):  # новый
        query = self.request.GET.get('q')
        object_list = Music.objects.filter(
            Q(name__icontains=query) | Q(state__icontains=query)
        )
        return object_list
